package pl.dolowy.greentesla.atmservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.dolowy.greentesla.atmservice.model.ATM;
import pl.dolowy.greentesla.atmservice.service.ATMService;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class ATMController {

    private final ATMService atmService;

    @GetMapping("/atms/calculateOrder")
    public List<ATM> prepareServiceOrders(@RequestBody List<ATM> atms) {
        return atmService.prepareServiceOrders(atms);
    }
}
