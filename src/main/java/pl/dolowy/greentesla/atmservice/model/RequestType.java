package pl.dolowy.greentesla.atmservice.model;

public enum RequestType {
    FAILURE_RESTART,
    PRIORITY,
    SIGNAL_LOW,
    STANDARD
}
