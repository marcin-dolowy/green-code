package pl.dolowy.greentesla.atmservice.service;

import org.springframework.stereotype.Service;
import pl.dolowy.greentesla.atmservice.model.ATM;

import java.util.Comparator;
import java.util.List;

@Service
public class ATMService {

    public List<ATM> prepareServiceOrders(List<ATM> atms) {
        return atms.stream()
                .distinct()
                .sorted(Comparator
                        .comparing(ATM::getRegion)
                        .thenComparing(ATM::getRequestType))
                .toList();
    }
}
