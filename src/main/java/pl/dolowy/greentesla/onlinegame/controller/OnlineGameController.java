package pl.dolowy.greentesla.onlinegame.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.dolowy.greentesla.onlinegame.model.Clan;
import pl.dolowy.greentesla.onlinegame.model.Game;
import pl.dolowy.greentesla.onlinegame.service.OnlineGameService;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class OnlineGameController {

    private final OnlineGameService onlineGameService;

    @GetMapping("/onlinegame/calculate")
    public List<List<Clan>> setClansOnTheBoard(@RequestBody Game game) {
        return onlineGameService.setClansOnTheBoard(game);
    }
}
