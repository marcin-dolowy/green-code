package pl.dolowy.greentesla.onlinegame.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Clan {
    private int numberOfPlayers;
    private int points;

}
