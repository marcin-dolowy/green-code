package pl.dolowy.greentesla.onlinegame.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Game {
    private int groupCount;
    private List<Clan> clans;
}
