package pl.dolowy.greentesla.onlinegame.service;

import org.springframework.stereotype.Service;
import pl.dolowy.greentesla.onlinegame.model.Clan;
import pl.dolowy.greentesla.onlinegame.model.Game;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OnlineGameService {

    public List<List<Clan>> setClansOnTheBoard(Game game) {

        LinkedList<Clan> clans = sortClans(game);
        List<List<Clan>> result = new ArrayList<>();
        int groupCount = game.getGroupCount();

        while (!clans.isEmpty()) {
            List<Clan> list = new ArrayList<>();
            int players = clans.get(0).getNumberOfPlayers();
            list.add(clans.removeFirst());
            if (players == groupCount) {
                result.add(list);
            } else {
                for (Clan clan : clans) {
                    if (players + clan.getNumberOfPlayers() <= groupCount) {
                        players += clan.getNumberOfPlayers();
                        list.add(clan);
                    }
                }
                result.add(list);
                clans.removeAll(list);
            }
        }
        return result;
    }

    private LinkedList<Clan> sortClans(Game game) {
        return game.getClans()
                .stream()
                .sorted(
                        Comparator.comparing(Clan::getPoints, Comparator.reverseOrder())
                                .thenComparing(Clan::getNumberOfPlayers)
                ).collect(Collectors.toCollection(LinkedList::new));
    }
}

