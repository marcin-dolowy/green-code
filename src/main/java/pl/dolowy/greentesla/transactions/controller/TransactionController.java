package pl.dolowy.greentesla.transactions.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.dolowy.greentesla.transactions.model.Account;
import pl.dolowy.greentesla.transactions.model.Transaction;
import pl.dolowy.greentesla.transactions.service.TransactionsService;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class TransactionController {

    private final TransactionsService transactionsService;

    @GetMapping("/transactions/report")
    public List<Account> getAccountsList(@RequestBody List<Transaction> transactions) {
        return transactionsService.getAccountsList(transactions);

    }
}
