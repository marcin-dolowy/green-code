package pl.dolowy.greentesla.transactions.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class Account {
    private String account;
    private int debitCount;
    private int creditCount;

    public BigDecimal balance;

    public Account(String creditAccountNumber) {
        this.account = creditAccountNumber;
        this.balance = BigDecimal.ZERO;
    }
}
