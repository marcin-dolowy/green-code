package pl.dolowy.greentesla.transactions.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Transaction {
    private String debitAccount;
    private String creditAccount;
    private BigDecimal amount;
}
