package pl.dolowy.greentesla.transactions.service;

import org.springframework.stereotype.Service;
import pl.dolowy.greentesla.transactions.model.Account;
import pl.dolowy.greentesla.transactions.model.Transaction;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class TransactionsService {

    public List<Account> getAccountsList(List<Transaction> transactions) {
        Map<String, Account> accountsMap = new HashMap<>();

        transactions.forEach(transaction -> updateAccount(accountsMap, transaction));

        return accountsMap.values()
                .stream()
                .parallel()
                .sorted(Comparator.comparing(Account::getAccount))
                .collect(Collectors.toList());
    }

    private static void updateAccount(Map<String, Account> accountsMap, Transaction transaction) {
        Account debitAccount = accountsMap.computeIfAbsent(transaction.getDebitAccount(), Account::new);
        debitAccount.setDebitCount(debitAccount.getDebitCount() + 1);
        debitAccount.setBalance(debitAccount.getBalance().subtract(transaction.getAmount()));

        Account creditAccount = accountsMap.computeIfAbsent(transaction.getCreditAccount(), Account::new);
        creditAccount.setCreditCount(creditAccount.getCreditCount() + 1);
        creditAccount.setBalance(creditAccount.getBalance().add(transaction.getAmount()));
    }

}
